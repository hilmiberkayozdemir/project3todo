import Rect from "react";


const Form=({setInputText,todos,setTodos,inputText})=>{


    // here I can write javascript code and function
const inputTextHandler=(e)=>{

    
    setInputText(e.target.value);
};

const submitTodoHandler=(e)=> {

    e.preventDefault();
    setTodos(
      [...todos, 
        {text:inputText,completed:false, id:Math.random() *1000}, ]);
        setInputText("");
};

    return(

<form>
      <input placeholder="add a new todo..." value={inputText} onChange={inputTextHandler} type="text" className="todo-input" />
      <button onClick={submitTodoHandler}  type="submit">
        Add
      </button>
    
    </form>

    );


};

export default Form;