import React,{useState}  from 'react';

import './App.css';


// importing componenets 
import Form from './components/form';
import TodoList from './components/todo_list';

function App() {

const[inputText, setInputText]= useState("");
const[todos,setTodos]=useState([]);


  return (
    <div className="App">
      <header>
      <h1>Todo List</h1>
    </header>
    
    <TodoList setTodos={setTodos} todos={todos}/>
    <Form 
    inputText={inputText}
     todos={todos}
      setTodos={setTodos} 
      setInputText={setInputText}/>
    

    </div>
  );
}

export default App;
